Schema to Graffle
=================

Uses the Applescript API to render a database model in
OmniGraffle.

Usage::

	Usage: schema_to_graffle.py [options]

	Options:
	  -h, --help       show this help message and exit
	  --app=APP        Application name
	  --uri=URI        Database uri
	  --tables=TABLES  List of table names to generate, separated by spaces
	  --schema=SCHEMA  Schema name

