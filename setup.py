from setuptools import setup
import os

readme = os.path.join(os.path.dirname(__file__), 'README.rst')

setup(name='schema_to_graffle',
      version="0.1",
      description="Render database schemas as OmniGraffle docuemnts",
      long_description=open(readme).read(),
      classifiers=[
      'Development Status :: 3 - Alpha',
      'Environment :: Console',
      'Intended Audience :: Developers',
      'Programming Language :: Python',
      'Programming Language :: Python :: 3',
      'Programming Language :: Python :: Implementation :: CPython',
      ],
      keywords='Blogofile',
      author='Mike Bayer',
      author_email='mike@zzzcomputing.com',
      license='MIT',
      py_modules=['schema_to_graffle'],
      zip_safe=False,
      install_requires=[
            'SQLAlchemy>=0.8.0',
            "appscript"
      ],
      entry_points={
        'console_scripts': ['schema_to_graffle = schema_to_graffle:main'],
      },
)
